const mongoose = require("mongoose");
const { Schema } = mongoose;

const categorySchema = new mongoose.Schema(
  {
    name: String,
    description: String,
    user: { type: Schema.Types.ObjectId, ref: "User" },
  },
  {
    toJSON: {
      virtuals: true,
      transform: (doc, ret) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

const Category = mongoose.model("Category", categorySchema);

module.exports = Category;
