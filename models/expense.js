const mongoose = require("mongoose");
const { Schema } = mongoose;
const TransationTypeEnum = require("../enums/transation_type");
const CurrencyEnum = require("../enums/currency");

const expenseSchema = new mongoose.Schema(
  {
    name: String,
    description: String,
    transaction_type: {
      type: String,
      enum: Object.values(TransationTypeEnum),
      default: TransationTypeEnum.EXPENSE,
    },
    amount: Number,
    currency: {
      type: String,
      enum: Object.values(CurrencyEnum),
      default: CurrencyEnum.MMK,
    },
    user: { type: Schema.Types.ObjectId, ref: "User" },
    category: { type: Schema.Types.ObjectId, ref: "Category" },
    date: Date,
    created_at: {
      type: Date,
      default: Date.now,
    },
  },
  {
    toJSON: {
      virtuals: true,
      transform: (doc, ret) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  }
);

const Expense = mongoose.model("Expense", expenseSchema);

module.exports = Expense;
