const Currency = Object.freeze({
  MMK: "MMK",
  USD: "USD",
  THB: "THB",
});

module.exports = Currency;
