const TransationType = Object.freeze({
  EXPENSE: "expense",
  INCOME: "income",
});

module.exports = TransationType;
