const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const app = express();
const port = 3000;

//middleware
app.use(bodyParser.json());

//routes

const apiRoutes = require("./routes/api");
app.use("/api", apiRoutes);
app.use("/", async (req, res) =>{
  res.json({
      message: "welcome daily expense"
  });
});

//database
main().catch((err) => console.log(err));
async function main() {
  // User.create({
  //   name: "Kamar",
  //   email: "kamar@gmail.com",
  // });
  await mongoose.connect("mongodb://mongo_db:27017/daily_expense");
  // use `await mongoose.connect('mongodb://user:password@127.0.0.1:27017/test');` if your database has auth enabled
}

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
