const express = require("express");
const router = express.Router();
const ExpenseController = require("../../controllers/expense");

router.get("/", ExpenseController.getList);
router.post("/", ExpenseController.create);
router.put("/:id", ExpenseController.update);
router.get("/:id", ExpenseController.getDetail);
router.delete("/:id", ExpenseController.delete);

module.exports = router;
