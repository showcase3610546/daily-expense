const express = require("express");
const router = express.Router();
const CategoryController = require("../../controllers/category");

router.get("/", CategoryController.getList);
router.get("/:id", CategoryController.getDetail);
router.post("/", CategoryController.create);
router.put("/:id", CategoryController.update);
router.delete("/:id", CategoryController.delete);

module.exports = router;
