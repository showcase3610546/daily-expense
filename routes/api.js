const express = require("express");
const router = express.Router();
const authRoutes = require("./api/auth");
const categoryRoutes = require("./api/category");
const expenseRoutes = require("./api/expense");

const authToken = require("../middleware/auth_token");

router.use("/", authRoutes);
router.use("/categories", authToken, categoryRoutes);
router.use("/expenses", authToken, expenseRoutes);

module.exports = router;
