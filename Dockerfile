# Use the official Node.js image as the base image
FROM node:20.11.1

# Set working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json to the working directory
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code to the working directory
COPY . .

# Expose port(s) if your Node.js application requires
EXPOSE 3000

CMD ["npm","start"]


