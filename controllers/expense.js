const Category = require("../models/category");
const User = require("../models/user");
const Expense = require("../models/expense");
const mongoose = require("mongoose");

exports.getList = async (req, res) => {
  const expenses = await Expense.find({ user: req.user.userId })
    .populate(["user", "category"])
    .exec();
  const totalExpense = await Expense.aggregate([
    {
      $match: {
        user: new mongoose.Types.ObjectId(req.user.userId),
        transaction_type: "expense",
      },
    },
    {
      $group: {
        _id: null,
        totalAmount: { $sum: "$amount" },
      },
    },
  ]);
  const totalIncome = await Expense.aggregate([
    {
      $match: {
        user: new mongoose.Types.ObjectId(req.user.userId),
        transaction_type: "income",
      },
    },
    {
      $group: {
        _id: null,
        totalAmount: { $sum: "$amount" },
      },
    },
  ]);
  res.json({
    message: "Sucessfully retrieve expense list.",
    total_income: totalIncome[0] ? totalIncome[0].totalAmount : 0,
    total_expense: totalExpense[0] ? totalExpense[0].totalAmount : 0,
    expenses: expenses,
  });
};

exports.create = async (req, res) => {
  const expense = new Expense({
    name: req.body.name,
    description: req.body.description,
    transaction_type: req.body.transaction_type,
    amount: req.body.amount,
    currency: req.body.currency,
    user: req.user.userId,
    category: req.body.category,
    date: req.body.date,
  });
  await expense.save();
  res.json({
    message: "Sucessfully create expense.",
    expense: expense,
  });
};

exports.update = async (req, res) => {
  const expense = await Expense.findById(req.params.id);

  expense.name = req.body.name;
  expense.description = req.body.description;
  expense.transaction_type = req.body.transaction_type;
  expense.amount = req.body.amount;
  expense.currency = req.body.currency;
  expense.category = req.body.category;
  expense.date = req.body.date;
  expense.user = req.user.userId;
  await expense.save();
  res.json({
    message: "Sucessfully update expense.",
    expense: expense,
  });
};

exports.getDetail = async (req, res) => {
  const expense = await Expense.findById(req.params.id)
    .populate(["user", "category"])
    .exec();
  res.json({
    message: "Sucessfully retrieve expense.",
    expense: expense,
  });
};

exports.delete = async (req, res) => {
  await Expense.deleteOne({ _id: req.params.id });
  res.json({
    message: "Sucessfully delete expense.",
  });
};
