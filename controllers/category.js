const Category = require("../models/category");
const User = require("../models/user");

exports.getList = async (req, res) => {
  const categories = await Category.find({ user: req.user.userId });
  res.json({
    message: "Sucessfully retrieve category list.",
    categories: categories,
  });
};

exports.getDetail = async (req, res) => {
  const category = await Category.findById(req.params.id)
    .populate("user")
    .exec();
  res.json({
    message: "Sucessfully retrieve category.",
    categories: category,
  });
};

exports.create = async (req, res) => {
  const category = new Category({
    name: req.body.name,
    description: req.body.description,
    user: req.user.userId,
  });
  await category.save();
  res.json({
    message: "Sucessfully create category.",
    category: category,
  });
};

exports.update = async (req, res) => {
  const category = await Category.findById(req.params.id);

  category.name = req.body.name;
  category.description = req.body.description;
  category.user = req.user.userId;
  await category.save();
  res.json({
    message: "Sucessfully update category.",
    category: category,
  });
};

exports.delete = async (req, res) => {
  await Category.deleteOne({ _id: req.params.id });
  res.json({
    message: "Sucessfully delete category.",
  });
};
