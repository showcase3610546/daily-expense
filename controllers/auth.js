const User = require("../models/user");
const jwt = require("jsonwebtoken");

exports.login = async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email });
  //   res.json(user);
  // Replace this with your actual authentication logic
  if (password === user.password) {
    const token = jwt.sign({ userId: user.id, email: user.email }, "secretKey");
    res.json({
      message: "Successfully login",
      token: token,
      user: user,
    });
  } else {
    res.status(401).json({ message: "Authentication failed" });
  }
};
